# Feline Freinds Foster Apply Page
# www.felinefriendschicago.org
# Purpose: Convert curent application system (print and hand write on a pdf sheet)
# to an online form process 
# Type: HTML5 (W3S validated)
# Encoding: UTF-8
# Disclaimer: This is a purely non-compensated volunteer initiative. It does
# not reflect my current employer, is not related to any professional
# contracts, and is entirely my own work completed using my own personal time
# and resources.
# Coded manually in Sublime (www.sublimetext.com) on Mac OS 10.9.2
